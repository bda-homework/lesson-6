package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Employee employee1 = new Employee(1, "Murad Shukurzadeh", "Hi-Fi", "Finance", "Accountant", 500.00);
        Employee employee2 = new Employee(2, "Fikirat Javadov", "AutoStockUp LLC", "IT", "Front-end Developer", 4600);
        employee1.employeeMelumat();
        employee2.employeeMelumat();
    }

    static class Employee {
        private int id;
        private String fullName;
        private String company;
        private String department;
        private String position;
        private double salary;

        public Employee(int id, String fullName, String company, String department, String position, double salary) {
            this.id = id;
            this.fullName = fullName;
            this.company = company;
            this.department = department;
            this.position = position;
            this.salary = salary;
        }

        public void employeeMelumat () {
            System.out.println("Employee ID: " + id);
            System.out.println("fullName: " + fullName);
            System.out.println("company: " + company);
            System.out.println("department: " + department);
            System.out.println("position: " + position);
            System.out.println("salary: " + salary);
        }
    }

}