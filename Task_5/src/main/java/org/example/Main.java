package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Fruits fruits = new Fruits(5, 5, 5);
        Vegetables vegetables = new Vegetables(7, 7, 7);
        System.out.println("meyvenin aciligi: " + fruits.aciliq);
        System.out.println("meyvenin sirinliyi: " + fruits.sirinlik);
        System.out.println("meyvenin turslugu: " + fruits.tursluq);
        System.out.println("terevezin aciligi: " + vegetables.aciliq);
        System.out.println("terevezin sirinliyi: " + vegetables.sirinlik);
        System.out.println("terevezin turslugu: " + vegetables.tursluq);
    }

    static class Fruits {
        private int sirinlik;
        private int tursluq;
        private int aciliq;

        public Fruits(int sirinlik, int tursluq, int aciliq) {
            this.sirinlik = sirinlik;
            this.tursluq = tursluq;
            this.aciliq = aciliq;
        }

        public Fruits() {

        }
    }

    static class Vegetables {
        private int sirinlik;
        private int tursluq;
        private int aciliq;

        public Vegetables(int sirinlik, int tursluq, int aciliq) {
            this.sirinlik = sirinlik;
            this.tursluq = tursluq;
            this.aciliq = aciliq;
        }

        public Vegetables() {

        }
    }

}