package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static class Esas {
        public static void main(String[] args) {
            DigerClass obj = new DigerClass();

            obj.digerClass();
        }
    }

    static class DigerClass {
        public void digerClass() {
            System.out.println("Umid varam ki, duz anladim");
        }
    }

}