package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Fruits fruit = new Fruits();
        Vegetables vegetable = new Vegetables();

        fruit.fruits();
        vegetable.vegetables();
    }

    static class Fruits {
        public static void fruits() {
            System.out.println("alma");
        }
    }

    static class Vegetables {
        public static void vegetables() {
            System.out.println("pomidor");
        }
    }

}