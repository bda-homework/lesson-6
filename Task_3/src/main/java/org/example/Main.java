package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        User user = new User("Murad", "Shukurzade", 28);
        user.UserMelumat();
    }

    static class User {
        private String name;

        private String surName;

        private int age;

        public User(String name, String surName, int age) {
            this.name = name;
            this.surName = surName;
            this.age = age;
        }

        public void UserMelumat() {
            System.out.println("userin adi: " + name);
            System.out.println("userin soyadi: " + surName);
            System.out.println("usern yashi: " + age);
        }

    }

}